;; -*-emacs-lisp-*-
;;
;; Emacs startup file for the Debian GNU/Linux tpp package
;;
;; Originally contributed by Nils Naumann <naumann@unileoben.ac.at>
;; Modified by Dirk Eddelbuettel <edd@debian.org>
;; Adapted for dh-make by Jim Van Zandt <jrv@vanzandt.mv.com>
;; Adapted for tpp by Axel Beckert <abe@debian.org>

;; The tpp package follows the Debian/GNU Linux 'emacsen' policy and
;; byte-compiles its elisp files for each 'emacs flavor' (emacs23,
;; emacs24, xemacs21, …).  The compiled code is then installed in a
;; subdirectory of the respective site-lisp directory.
;; We have to add this to the load-path:

;; Set up to autoload
(autoload 'tpp-mode "tpp-mode" "Major mode for editing TPP slides" t)
(setq auto-mode-alist
     (cons '("\\.tpp\\'" . tpp-mode) auto-mode-alist))
