TARGETS = live-config mountkernfs.sh hostname.sh eudev keyboard-setup.sh mountdevsubfs.sh checkroot.sh cryptdisks cryptdisks-early mountall.sh mountall-bootclean.sh mountnfs.sh mountnfs-bootclean.sh networking urandom ufw alsa-utils live-tools bootmisc.sh checkfs.sh checkroot-bootclean.sh screen-cleanup procps lm-sensors kmod pppd-dns x11-common
INTERACTIVE = eudev keyboard-setup.sh checkroot.sh cryptdisks cryptdisks-early live-tools checkfs.sh
mountkernfs.sh: live-config
eudev: mountkernfs.sh
keyboard-setup.sh: mountkernfs.sh
mountdevsubfs.sh: mountkernfs.sh
checkroot.sh: keyboard-setup.sh mountdevsubfs.sh hostname.sh
cryptdisks: checkroot.sh cryptdisks-early
cryptdisks-early: checkroot.sh
mountall.sh: checkfs.sh checkroot-bootclean.sh
mountall-bootclean.sh: mountall.sh
mountnfs.sh: mountall.sh mountall-bootclean.sh networking
mountnfs-bootclean.sh: mountall.sh mountall-bootclean.sh mountnfs.sh
networking: mountkernfs.sh mountall.sh mountall-bootclean.sh urandom procps
urandom: mountall.sh mountall-bootclean.sh
ufw: mountall.sh mountall-bootclean.sh
alsa-utils: mountall.sh mountall-bootclean.sh mountnfs.sh mountnfs-bootclean.sh
live-tools: bootmisc.sh mountall.sh mountall-bootclean.sh
bootmisc.sh: mountall-bootclean.sh checkroot-bootclean.sh mountall.sh mountnfs.sh mountnfs-bootclean.sh
checkfs.sh: cryptdisks checkroot.sh
checkroot-bootclean.sh: checkroot.sh
screen-cleanup: mountall.sh mountall-bootclean.sh mountnfs.sh mountnfs-bootclean.sh
procps: mountkernfs.sh mountall.sh mountall-bootclean.sh
lm-sensors: mountall.sh mountall-bootclean.sh mountnfs.sh mountnfs-bootclean.sh
kmod: checkroot.sh
pppd-dns: mountall.sh mountall-bootclean.sh
x11-common: mountall.sh mountall-bootclean.sh mountnfs.sh mountnfs-bootclean.sh
